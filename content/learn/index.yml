---
  title: "GitLab Learn"
  description: "Learn new skills while working with GitLab. Our easy-to-use learning platform provides instructions and feedback throughout your journey."
  certifications_link:
    title: "Learn more about GitLab Certifications"
    link: "https://about.gitlab.com/learn/certifications/public/"
    data_ga_name: "learn more"
    data_ga_location: "header"
  free_trial_link:
    title: "Get free trial"
    link: "https://gitlab.com/-/trials/new?_gl=1%2Aiun8xs%2A_ga%2AMjYyMTI5MzcuMTYyNjk3NTQwMg..%2A_ga_ENFH3X7M5Y%2AMTY2MzY5MTAzNi43Mi4wLjE2NjM2OTEwMzYuMC4wLjA.&glm_content=default-saas-trial&glm_source=about.gitlab.com"
    data_ga_name: "free trial"
    data_ga_location: "header"
  reset_all: "Reset all"
  use_case: "Use-case"
  level: "Level"
  pace: "Self-paced"
  assessment: "Assessment"
  registration: "Registration"
  confidentiality: "Confidentiality"
  maintainer: "Maintainer"
  programs:
    - name: "TeamOps Certification"
      url: https://levelup.gitlab.com/learn/course/teamops
      description: |
        TeamOps is a new people practice that brings precision and operations to how people work together.
      live_date: "2022-10-01"
      maintainer: "Learning & Development"
      level: "Intermediate"
      use_case: "Talent & Development"
      pace: "Self-Paced"
      assessment: "Certification"
      registration: "Free"
      confidentiality: "Public"

    - name: "Getting Started with GitLab Learning Path"
      url: https://levelup.gitlab.com/learn/learning-path/getting-started-with-gitlab
      description: This learning path combine a series of courses to teach you GitLab skills.
      live_date: "2022-12-29"
      maintainer: "PS Education Services"
      level: "Intermediate"
      use_case: "GitLab Basics"
      pace: "Self-Paced"
      assessment: "Badge"
      registration: "Free"
      confidentiality: "Public"

    - name: "Get started with GitLab tutorials"
      url: https://docs.gitlab.com/ee/tutorials/
      description: |
        Get started with GitLab tutorials is a handpicked set of tutorials and resources to help you learn how to use GitLab. It includes topics on Finding your way around GitLab, Use Git, Plan your work in projects, Use CI/CD pipelines, and Secure your application.
      live_date: "2022-01-13"
      maintainer: "Technical Writing"
      level: "Beginner"
      use_case: "GitLab Basics"
      pace: "Self-Paced"
      assessment: "None"
      registration: "Free"
      confidentiality: "Public"

    - name: "DevOps with GitLab CI Course - Build Pipelines and Deploy to AWS"
      url: https://levelup.gitlab.com/learn/course/devops-with-gitlab-ci-course-build-pipelines-and-deploy-to-aws
      description: |
        This FreeCodeCamp course will teach you how to use GitLab CI to create CI/CD pipelines for building and deploying software to AWS. Original author is Valentin Despa, GitLab hero, who collaborated and reviewed together with Michael Friedrich on the Developer Evcangelism team.
      live_date:
      maintainer: "Developer Evangelism"
      level: "Beginner"
      use_case: "DevOps"
      pace: "Self-Paced"
      assessment: "None"
      registration: "None"
      confidentiality: "Public"

    - name: "Technical Writing Fundamentals"
      url: https://levelup.gitlab.com/learn/course/gitlab-technical-writing-fundamentals
      description: |
        The GitLab Technical Writing Fundamentals course is available to help both GitLab and community contributors write and edit documentation for the product. This course provides direction on grammar, writing, and topic design.
      live_date: "2021-08-02"
      maintainer: "Engineering"
      level: "Beginner"
      use_case: "Talent & Development"
      pace: "Self-Paced"
      assessment: "None"
      registration: "Free"
      confidentiality: "Public"

    - name: "Backwards Compatibility Training"
      url: https://levelup.gitlab.com/learn/course/backwards-compatibility-training
      description: With multi-node setups, at any given time two versions of the application can run, the code needs to be backward compatible in order to support zero-downtime upgrades. Learn about the common causes of compatibility problems during upgrades and best practices to prevent them.
      live_date:
      maintainer: "Engineering"
      level: "Beginner"
      use_case: "Version Control & Collaboration"
      pace: "Self-Paced"
      assessment: "None"
      registration: "Free"
      confidentiality: "Public"

    - name: "GitLab 101"
      url: https://levelup.gitlab.com/learn/course/gitlab101
      description: |
        This training is geared toward GitLab team members and the wider community who are in non-engineering roles (i.e. recruiting, peopleops, marketing, finance, etc) and/or have not used a DevOps tool like GitLab before. This can also be helpful for non-engineering people outside of GitLab wanting to learn how to use GitLab for personal projects.
      live_date: "2020-02-01"
      maintainer: "Learning & Development"
      level: "Beginner"
      use_case: "GitLab Basics"
      pace: "Self-Paced"
      assessment: "Badge"
      registration: "Free"
      confidentiality: "Public"

    - name: "GitLab 201"
      url: https://levelup.gitlab.com/learn/course/gitlab-201-certification
      description: |
        This training is a deep dive into GitLab Epics and Merge requests for team members and wider community members getting started using the GitLab tool
      live_date: "2020-08-01"
      maintainer: "Learning & Development"
      level: "Intermediate"
      use_case: "GitLab Basics"
      pace: "Self-Paced"
      assessment: "Badge"
      registration: "Free"
      confidentiality: "Public"

    - name: "Remote Foundations Certification"
      url: https://levelup.gitlab.com/learn/course/remote-foundations
      description: |
        Presented at a self-directed pace, the GitLab remote certification is designed to give new managers and individual contributors an opportunity to master all-remote business concepts and build key skills in remote subject areas.
      live_date: "2021-01-15"
      maintainer: "Learning & Development"
      level: "Beginner"
      use_case: "Talent & Development"
      pace: "Self-Paced"
      assessment: "Certification"
      registration: "Free"
      confidentiality: "Public"

    - name: "GitLab Certified Git Associate"
      url: https://about.gitlab.com/services/education/gitlab-certified-associate/
      description: GitLab Certified Git Associate is a technical certification offered by GitLab Education Services to help the GitLab community and team members validate their ability to apply GitLab in their daily DevOps work. 
      live_date: "2020-04-01"
      maintainer: "PS Education Services"
      level: "Beginner"
      use_case: "DevOps"
      pace: "Both"
      assessment: "Certification"
      registration: "Paid"
      confidentiality: "Public"

    - name: "GitLab Certified CI/CD Associate"
      url: https://about.gitlab.com/services/education/gitlab-cicd-associate/
      description: GitLab Certified CI/CD Associate is a technical certification offered by GitLab Education Services to help the GitLab community and team members validate their ability to apply GitLab concurrent DevOps processes and best practices in their daily work.
      live_date: "2020-06-01"
      maintainer: "PS Education Services"
      level: "Intermediate"
      use_case: "Continuous Integration"
      pace: "Both"
      assessment: "Certification"
      registration: "Paid"
      confidentiality: "Public"

    - name: "GitLab Certified DevOps Professional"
      url: https://about.gitlab.com/services/education/gitlab-certified-devops-pro/
      description: GitLab Certified DevOps Professional is a technical certification offered by GitLab Education Services to help the GitLab community and team members validate their ability to apply essential GitLab capabilities across a broad range of DevOps stages and functions.
      live_date: "2020-12-01"
      maintainer: "PS Education Services"
      level: "Intermediate"
      use_case: "DevOps"
      pace: "Both"
      assessment: "Certification"
      registration: "Paid"
      confidentiality: "Public"

    - name: "GitLab Certified Project Management Associate"
      url: https://about.gitlab.com/services/education/gitlab-project-management-associate/
      description: GitLab Certified Project Management Associate is a technical certification offered by GitLab Education Services to help the GitLab community and team members validate their ability to use GitLab for lean and agile project management, from basic issue tracking to scrum and kanban style project management throughout the complete DevOps lifecycle.
      live_date: "2021-10-27"
      maintainer: "PS Education Services"
      level: "Intermediate"
      use_case: "Agile Planning"
      pace: "Both"
      assessment: "Certification"
      registration: "Paid"
      confidentiality: "Public"

    - name: "GitLab Certified Security Specialist"
      url: https://about.gitlab.com/services/education/gitlab-security-specialist/
      description: GitLab Certified Security Specialist is a technical certification offered by GitLab Education Services to help the GitLab community and team members validate their ability to use GitLab for secure software development through continuous security testing, scanning, and remediation.
      live_date: "2021-10-27"
      maintainer: "PS Education Services"
      level: "Intermediate"
      use_case: "DevSecOps"
      pace: "Both"
      assessment: "Certification"
      registration: "Paid"
      confidentiality: "Public"

    - name: "GitLab with Git Basics Training"
      url: https://about.gitlab.com/services/education/gitlab-basics/
      description: This class provides users with an introduction to GitLab and Git. It starts with an overview of GitLab so you can learn the basics about what GitLab does and why DevOps teams use it. 
      live_date: "2020-01-01"
      maintainer: "PS Education Services"
      level: "Beginner"
      use_case: "GitLab Basics"
      pace: "Instructor-Led"
      assessment: "None"
      registration: "Paid"
      confidentiality: "Public"

    - name: "GitLab CI/CD Training"
      url: https://about.gitlab.com/services/education/gitlab-ci/
      description: This class explains what Continuous Integration/Continuous Deployment (CI/CD) pipelines are and what value they bring to the software development lifecycle. It also outlines the architecture behind GitLab's CI/CD pipelines and explains how to set up basic CI/CD pipelines in your own projects.
      live_date: "2020-01-01"
      maintainer: "PS Education Services"
      level: "Intermediate"
      use_case: "Continuous Delivery/Release"
      pace: "Instructor-Led"
      assessment: "None"
      registration: "Paid"
      confidentiality: "Public"

    - name: "GitLab for Project Managers Training"
      url: https://about.gitlab.com/services/education/pm/
      description: This class introduces users to GitLab's Plan stage, where they can manage software products or other projects. It focuses on the various tools available, including issues, epics, milestones, iterations, labels, roadmaps, burndown charts, and boards.
      live_date: "2020-01-01"
      maintainer: "PS Education Services"
      level: "Intermediate"
      use_case: "Agile Planning"
      pace: "Instructor-Led"
      assessment: "None"
      registration: "Paid"
      confidentiality: "Public"

    - name: "GitLab Security Essentials Training"
      url: https://about.gitlab.com/services/education/security-essentials/
      description: This course covers all of the essential security capabilities of GitLab, including Static Application Security Testing, secret detection, Dynamic Application Security Testing, dependency scanning, container scanning, license compliance, and fuzz testing.
      live_date: "2020-12-01"
      maintainer: "PS Education Services"
      level: "Intermediate"
      use_case: "DevSecOps"
      pace: "Instructor-Led"
      assessment: "None"
      registration: "Paid"
      confidentiality: "Public"

    - name: "GitLab DevOps Fundamentals Training"
      url: https://about.gitlab.com/services/education/devops-fundamentals/
      description: Concurrent DevOps is a new way of thinking about how to create and ship software. Rather than organizing work in a sequence of steps and handoffs, the power of working concurrently is in unleashing collaboration across the organization.
      live_date: "2020-01-01"
      maintainer: "PS Education Services"
      level: "Advanced"
      use_case: "DevOps"
      pace: "Instructor-Led"
      assessment: "None"
      registration: "Paid"
      confidentiality: "Public"

    - name: "GitLab System Administration Training"
      url: https://about.gitlab.com/services/education/admin/
      description: This course covers installation, configuration and maintenance tasks for a GitLab self-managed instance. This course is not intended for gitlab.com (SaaS) customers.
      live_date: "2020-02-01"
      maintainer: "PS Education Services"
      level: "Advanced"
      use_case:
      pace: "Instructor-Led"
      assessment: "None"
      registration: "Paid"
      confidentiality: "Public"

    - name: "GitLab Implementation Services Specialist Certification"
      url: https://about.gitlab.com/services/pse-certifications/implementation-specialist/
      description: GitLab Implementation Specialist is a technical certification offered by GitLab to help Partners and team members validate their ability to deploy, configure and provide stategic operational consulting to customers.
      live_date: "2021-10-27"
      maintainer: "PS Education Services"
      level: "Intermediate"
      use_case: 
      pace: "Self-Paced"
      assessment: "Certification"
      registration: "Paid"
      confidentiality: "Internal"

    - name: "GitLab Migration Services Specialist Certification"
      url: https://about.gitlab.com/handbook/customer-success/professional-services-engineering/gitlab-certified-migration-services-engineer/
      description: To be able to scale the availaility of GitLab Migration Services, GitLab Education Services offers a certification process for engineers who want to deliver this service to customers. The program is currently available to GitLab team members as well as selected GitLab Certified Services Partners.
      live_date: "2021-10-27"
      maintainer: "PS Education Services"
      level: "Intermediate"
      use_case: 
      pace: "Self-Paced"
      assessment: "Certification"
      registration: "Paid"
      confidentiality: "Internal"

    - name: "GitLab with Git Essentials"
      url: https://levelup.gitlab.com/learn/course/gitlab-with-git-essentials
      description: This course will provide you with an introduction to the GitLab system and how it uses Git to transform your DevOps processes.
      live_date: "2022-12-29"
      maintainer: "PS Education Services"
      level: "Beginner"
      use_case: "GitLab Basics"
      pace: "Self-Paced"
      assessment: "Badge"
      registration: "Free"
      confidentiality: "Public"

    - name: "GitLab:Agile Project Management"
      url: https://levelup.gitlab.com/learn/course/gitlab-agile-project-management
      description: This course introduces users to GitLab's Plan stage, where they can manage software products or other projects.
      live_date: "2022-12-29"
      maintainer: "PS Education Services"
      level: "Intermediate"
      use_case: "Agile Planning"
      pace: "Self-Paced"
      assessment: "Badge"
      registration: "Free"
      confidentiality: "Public"

    - name: "CI/CD with GitLab"
      url: https://levelup.gitlab.com/learn/course/continuous-integration-and-delivery-ci-cd-with-gitlab
      description: This course explains what Continuous Integration/Continuous Deployment (CI/CD) pipelines are and what value they bring to the software development lifecycle. It also outlines the architecture behind GitLab's CI/CD pipelines and explains how to set up basic CI/CD pipelines in your own projects.
      live_date: "2022-12-29"
      maintainer: "PS Education Services"
      level: "Intermediate"
      use_case: "Continuous Integration"
      pace: "Self-Paced"
      assessment: "Badge"
      registration: "Free"
      confidentiality: "Public"

    - name: "GitLab Security Essentials"
      url: https://levelup.gitlab.com/learn/course/security-essentials
      description: This course covers all of the essential security capabilities of GitLab, including Static Application Security Testing, secret detection, Dynamic Application Security Testing, dependency scanning, container scanning, license compliance, and fuzz testing.
      live_date: "2022-12-29"
      maintainer: "PS Education Services"
      level: "Intermediate"
      use_case: "DevSecOps"
      pace: "Self-Paced"
      assessment: "Badge"
      registration: "Free"
      confidentiality: "Public"
      
